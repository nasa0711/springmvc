package com.sda.SpringBootMVC.springBootMVC.repository;

import com.sda.SpringBootMVC.springBootMVC.domain.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
}
